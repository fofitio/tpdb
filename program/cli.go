package main

import (
	"database/sql"
	"fmt"
	"log"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"time"

	"./BoltDB"
	"./SqlDB"
)

var nombreBase string = "tarjeta_credito"

//stored producered Autorizar compra
var SPAutorizar string =` create function autorizar_compra(nrotarjeta_ingresada char, codseguridad_ingresado char, nrocomercio_ingresado int, monto_ingresado decimal) returns boolean as $$

declare
	aux record;

begin

        --verifica que la tarjeta no este suspendida
         select nrotarjeta into aux from tarjeta where nrotarjeta= nrotarjeta_ingresada and estado!= 'suspendida';

        if not found then 
	        insert into rechazo values (default,nrotarjeta_ingresada,nrocomercio_ingresado,transaction_timestamp(),monto_ingresado,'la tarjeta se encuentra suspendida.');
                return false;
        end if;

        --verifica que el numero de tarjeta sea vigente
        select nrotarjeta into aux from tarjeta where nrotarjeta= nrotarjeta_ingresada and estado= 'vigente';

        if not found then 
             insert into rechazo values (default,nrotarjeta_ingresada,nrocomercio_ingresado,transaction_timestamp(),monto_ingresado,'tarjeta no válida ó no vigente');
             return false;
        end if;

        --se verifica que el codigo de seguridad sea correcto
        select codseguridad into aux from tarjeta where nrotarjeta= nrotarjeta_ingresada and codseguridad= codseguridad_ingresado; 
                
        if not found then
	        insert into rechazo values (default,nrotarjeta_ingresada,nrocomercio_ingresado,transaction_timestamp(),monto_ingresado,'código de seguridad inválido.');
                return false;
        end if;

        --verifica que no supere el limite de la tarjeta
        select limitecompra into aux from tarjeta where nrotarjeta= nrotarjeta_ingresada and codseguridad= codseguridad_ingresado and monto_ingresado<limitecompra; 
                
        if not found then
	        insert into rechazo values (default,nrotarjeta_ingresada,nrocomercio_ingresado,transaction_timestamp(),monto_ingresado,'supera el limite de tarjeta');
                return false;
        end if;

        --verifica que la tarjeta no este vencida
       select nrotarjeta into aux from tarjeta where nrotarjeta= nrotarjeta_ingresada and cast(to_char(current_date,'yyyymm') as int) <= cast(validahasta as int) ; 
        
        if not found then 
	        insert into rechazo values (default,nrotarjeta_ingresada,nrocomercio_ingresado,transaction_timestamp(),monto_ingresado,'plazo de vigencia expirado.');
                return false;
        end if;

        --si paso es valida la tarjeta, realiza la compra
        insert into compra values (default,nrotarjeta_ingresada,nrocomercio_ingresado,transaction_timestamp(),monto_ingresado,true);
        return true;
end; 

$$ language plpgsql;`


//stored producered generar resumen
var SPGuardarRechazo string = `create or replace function guardar_rechazo(motivo varchar,nro_tarjeta char, 
	codigo_seguridad char,nro_comercio int,
	monto decimal) returns boolean as $$
begin

insert into rechazo (nrotarjeta,nrocomercio,fecha,monto,motivo) 
values (nro_tarjeta,nro_comercio,now(),monto,motivo);

return true;
exception when others then 
raise notice '% %', SQLERRM, SQLSTATE;
--raise notice 'fallo rechazo';
return false;    
end;
$$ language plpgsql`

var SPGenerarResumen string = `create or replace function generar_resumen(nrocliente_ingresado int, periodo int) returns void as $$
declare	
	nrocliente_guardado cliente%rowtype; --para que tenga el mismo tiṕo que cliente
	cierre_guardado cierre%rowtype;--para que tenga el mismo tiṕo que cierre
    comercio_guardado comercio%rowtype;
	t tarjeta%rowtype;
	c compra%rowtype;    	
    monto_total decimal;
	contador int :=1;
	resu int;

begin
    
	--guardo todos los datos de un determinado cliente en nrocliente_guardado
	select * into nrocliente_guardado from cliente where nrocliente = nrocliente_ingresado;
    
	--recorro con for la tala tarjeta que tenga el mismo cliente, por si el cliente tiene mas de una tarjeta
	for t in select * from tarjeta where nrocliente = nrocliente_ingresado  loop
 	
	--ver terminacion de tarjeta con esto cierrs y compra
		select * into cierre_guardado from cierre where terminacion= cast(right(cast(t.nrotarjeta as varchar),1) as int) and mes=periodo;

	    --con la tarjeta en la que estoy paradad saco el montototal a pagar  entre el inicio y cierre de tarjeta						
		select sum(monto)into monto_total from compra where nrotarjeta = t.nrotarjeta and cierre_guardado.fechainicio <= fecha and fecha <= cierre_guardado.fechacierre;
    
	    --hago innsert en cabecera de resumen
		insert into cabecera values(default,nrocliente_guardado.nombre, nrocliente_guardado.apellido, nrocliente_guardado.domicilio, t.nrotarjeta, cierre_guardado.fechainicio, cierre_guardado.fechacierre, cierre_guardado.fechavto, monto_total);
		
		--ver las compras que realizo		
		for c in select * from compra where nrotarjeta = t.nrotarjeta and cierre_guardado.fechainicio <= fecha and fecha <= cierre_guardado.fechacierre loop
        
		--la compra pagada y no  pagada boolean true 

		    --con el nrodetarjeta con el que estamos recorriendo guardamos todos los comercios en el que se compro con la tarjeta
			select * into comercio_guardado from comercio where nrocomercio = c.nrocomercio;
	        
			--arreglar porque no muetra el nroderesumen!!!!!!
			--select nroresumen into resu from cabecera cab where nrotarjeta = t.nrotarjeta and periodo=cast(substring(cast(cab.hasta as VARCHAR) from 6 for 2)as int);
			
			insert into detalle (nrolinea,fecha,nombrecomercio,monto) values(contador,cast(substring(cast(c.fecha as VARCHAR) from 1 for 10)as DATE) ,comercio_guardado.nombre,c.monto);
			
			contador:= contador + 1 ;

		end loop;
	end loop;
end;
$$ language plpgsql;`

//TriggerAlerta stored producered
var TriggerAlerta string = `create or replace function tipo_rechazo() returns trigger as $$

declare

	cont int;

begin
	 
    select count(*)	into cont from rechazo where nrotarjeta = new.nrotarjeta and (SELECT fecha::date) = (SELECT now()::date) and motivo ='supera el limite de tarjeta';
	
	if(cont > 1) then 
		
        insert into alerta (nrotarjeta,fecha,nrorechazo,codalerta,descripcion) values(new.nrotarjeta,now(),new.nrorechazo,32,'La tarjeta fue supendida ya que tuvo dos rechazos por exceso de límite en el mismo día.');

		update tarjeta set estado = 'suspendida' where nrotarjeta = new.nrotarjeta;
	
    else
    
    	insert into alerta(nrotarjeta,fecha,nrorechazo,codalerta,descripcion) values (new.nrotarjeta,now(),new.nrorechazo,0,new.motivo);
	
    end if;
	return new;

end;
$$ language plpgsql;
create trigger tipo_rechazo
after insert or update on rechazo
for each row
execute procedure tipo_rechazo();`

//TriggerComprasAlerta stored producered
var TriggerComprasAlerta string = `create or replace function alerta_compra() returns trigger as $$

declare 
    fila record;

    cp_comercio varchar;-- dist a oscar--
    
    enviarAlerta boolean := false;
    
    diferencia_minutos int;

    codigo_alerta int:=-1;

    descripcion varchar;-- dist a oscar--

begin

    select codigopostal into cp_comercio from comercio where nrocomercio = new.nrocomercio;

    --vamos a recorrer la tabla que devuelve el select, este devuelve el comercio,codigopstal y fecha del mismo 
    for fila in select  cm.nrocomercio,cm.codigopostal,cp.fecha from compra cp,comercio cm where cm.nrocomercio = cp.nrocomercio and cp.nrotarjeta =new.nrotarjeta order by cp.fecha desc loop

      --la direfecia de dos fechas y le eztraemos los minutos y los guardamos  
        SELECT EXTRACT(MINUTE FROM age(new.fecha,fila.fecha)) into diferencia_minutos;
		
		if(fila.codigopostal <> cp_comercio and diferencia_minutos<5)then
        
            codigo_alerta := 5;
        
            descripcion := 'Se registraron dos compras en menos de 5 minutos en comercios con diferentes codigos postales';
        
        elsif(fila.codigopostal = cp_comercio and fila.nrocomercio <> new.nrocomercio and diferencia_minutos<1)then
        
            codigo_alerta := 1;
        
            descripcion := 'Se registraron dos compras en menos de 1 minuto en comercios distintos ubicados en el mismo codigo postal';           
        
        end if;

    end loop; 

    if(codigo_alerta <> 0)then

        insert into alerta (nrotarjeta,fecha,codalerta,descripcion) values (new.nrotarjeta,now(),codigo_alerta,descripcion); 
    end if; 

    return new;
end;
$$ language plpgsql;

create trigger alerta_compra
before insert or update on compra
for each row
execute procedure alerta_compra();`

//DropPkFk var
var DropPkFk string = `ALTER TABLE detalle DROP CONSTRAINT detalle_nroresumen_fk;
						 ALTER TABLE cabecera DROP CONSTRAINT cabecera_tarjeta_fk;
						 ALTER TABLE rechazo DROP CONSTRAINT rechazo_comercio_fk;
						 ALTER TABLE tarjeta DROP CONSTRAINT tarjeta_cliente_fk;
						 ALTER TABLE compra DROP CONSTRAINT compra_tarjeta_fk;
						 ALTER TABLE compra DROP CONSTRAINT compra_comercio_fk;
						 ALTER TABLE alerta DROP CONSTRAINT alerta_tarjeta_fk;
						 ALTER TABLE alerta DROP CONSTRAINT alerta_rechazo_fk;
						 ALTER TABLE consumo DROP CONSTRAINT consumo_tarjeta_fk;
						 ALTER TABLE consumo DROP CONSTRAINT consumo_comercio_fk;
						 ALTER TABLE cliente DROP CONSTRAINT cliente_pk;
						 ALTER TABLE tarjeta DROP CONSTRAINT tarjeta_pk;
						 ALTER TABLE compra DROP CONSTRAINT compra_pk;
						 ALTER TABLE comercio DROP CONSTRAINT comercio_pk;
						 ALTER TABLE rechazo DROP CONSTRAINT rechazo_pk;
						 ALTER TABLE alerta DROP CONSTRAINT alerta_pk;
						 ALTER TABLE cabecera DROP CONSTRAINT cabecera_pk;`

//CrearConsumo var
var CrearConsumo = `insert into consumo values(nrotarjeta,codseguridad,nrocomercio,monto);`

func ejecutar(procedimiento string) {
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname="+nombreBase+" sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	_, err = db.Exec(procedimiento)

	if err != nil {
		log.Fatal(err)
	}
}

func EliminarFkPk(){
	borrar(DropPkFk);
}

func borrar(procedimiento string) {
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname="+nombreBase+" sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	_, err = db.Exec(procedimiento)

	if err != nil {
		fmt.Printf("Ya se han borrado las primary y foreing key");
	}else {
		fmt.Printf("Se borraron las primary y foreing key con exito")
	}
}
func autorizar_compra(tarjeta string, codigoSeguridad string, numeroComercio int, monto float64) {
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname="+nombreBase+" sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	rows, err := db.Query("select autorizar_compra($1,$2,$3,$4)", tarjeta, codigoSeguridad, numeroComercio, monto)

	defer rows.Close()
	if err != nil {
		fmt.Printf("holis");
		log.Fatal(err)
	}
}
func ejecutarResumen(cliente int, periodo int) {
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname="+nombreBase+" sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	rows, err := db.Query("select generar_resumen($1,$2)", cliente, periodo)

	fmt.Printf("tarjeta:-%v", cliente)
	fmt.Printf("codigoseguridad:-%v", periodo)

	defer rows.Close()
	if err != nil {
		log.Fatal(err)
	}
}

func opcionConsumo() {
	var tarjeta string
	var codigoSeguridad string
	var numeroComercio int
	var monto float64
	fmt.Printf("Ingresá numero de tarjeta: ")
	fmt.Scanf("%v", &tarjeta)
	fmt.Printf("Ingresá numero de codigoSeguridad: ")
	fmt.Scanf("%v", &codigoSeguridad)
	fmt.Printf("Ingresá numero de numeroComercio: ")
	fmt.Scanf("%v", &numeroComercio)
	fmt.Printf("Ingresá numero de monto: ")
	fmt.Scanf("%v", &monto)
	CrearConsumo = strings.Replace(CrearConsumo, "nrotarjeta", tarjeta, -1)
	CrearConsumo = strings.Replace(CrearConsumo, "codseguridad", codigoSeguridad, -1)
	CrearConsumo = strings.Replace(CrearConsumo, "nrocomercio", strconv.Itoa(numeroComercio), -1)
	CrearConsumo = strings.Replace(CrearConsumo, "monto", strconv.FormatFloat(monto, 'E', -1, 64), -1)
	fmt.Printf(CrearConsumo);
	ejecutar(CrearConsumo)
	autorizar_compra(tarjeta, codigoSeguridad, numeroComercio, monto)
	CrearConsumo = `insert into consumo values(nrotarjeta,codseguridad,nrocomercio,monto);`
}
func opcionResumen() {
	var cliente int
	var periodo int
	fmt.Printf("Ingresá cliente: ")
	fmt.Scanf("%v", &cliente)
	fmt.Printf("Ingresá periodo: ")
	fmt.Scanf("%v", &periodo)
	ejecutarResumen(cliente, periodo)

}
func crearElementos() {
	ejecutar(SPGuardarRechazo)
	ejecutar(SPAutorizar)
	ejecutar(SPGenerarResumen)
	ejecutar(TriggerComprasAlerta)
	ejecutar(TriggerAlerta)
}
func crearBase() {
	err := SqlDB.CargarBaseCompleta()
	if err == nil {
		crearElementos()
	}

}
func clear() {
	cmd := exec.Command("clear")
	cmd.Stdout = os.Stdout
	cmd.Run()
}
func imprimirEncabezado() {
	fmt.Printf("***********************************************************")
	fmt.Printf("\n**Bienvenido al Sistema de control de tarjetas de credito**\n")
	fmt.Printf("***********************************************************\n")
	fmt.Println(time.Now().Format("2006-01-02 Hora: 15:04:05\n"))
}
func main() {

	bool := true
	crearBase()

	a := "a"
	b := "b"
	c := "c"
	d := "d"
	q := "q"

	clear()
	imprimirEncabezado()

	for bool {
		var ingreso string

		fmt.Printf("\nMenu principal:\n\n")
		fmt.Printf("\nIngrese una de la siguientes opciones:\n\n")
		fmt.Printf("		a: Ingresar Consumo\n")
		fmt.Printf("		b: Generar Resumen \n")
		fmt.Printf("		c: Eliminar Pk y Fks\n")
		fmt.Printf("		d: Ir a menu Nosql\n\n")
		fmt.Printf("		q: Salir\n")
		fmt.Printf("\nSu seleccion: ")

		fmt.Scanf("%s", &ingreso)

		switch ingreso {
		case a:
			opcionConsumo()
		case b:
			opcionResumen()
		case c:
			EliminarFkPk()
		case d:
			BoltDB.Menu()
		case q:
			bool = false
			fmt.Printf("Saliendo de la terminal\n")
		default:
			fmt.Printf("Opcion invalida")
		}

	}

}
