package SqlDB

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/lib/pq"
)

var nombreBase string = "tarjeta_credito"
var tablaCliente string = `create table cliente(
    					   nrocliente serial,
    					   nombre text,
    					   apellido text,
						   domicilio text,
						   telefono varchar(12)
						   );`

var tablaTarjeta string = `create table tarjeta(
							nrotarjeta char(16),
							nrocliente int,
							validadesde char(6), 
							validahasta char(6),
							codseguridad char(4),
							limitecompra decimal(8,2),
							estado char(10)
						);`

var tablaComercio string = `create table comercio(
							nrocomercio serial,
							nombre text,
							domicilio text,
							codigopostal char(8),
							telefono char(12)
						);`

var tablaCompra string = `create table compra(
							nrooperacion serial,
							nrotarjeta char(16),
							nrocomercio int,
							fecha timestamp,
							monto decimal(7,2),
							pagado boolean
						);`

var tablaRechazo string = `create table rechazo(
								nrorechazo serial,
								nrotarjeta char(16),
								nrocomercio int,
								fecha timestamp,
								monto decimal(7,2),
								motivo text
							);`
var tablaCierre string = `create table cierre(
								año int,
								mes int,
								terminacion int,
								fechainicio date,
								fechacierre date,
								fechavto date
							);`

var tablaCabecera string = `create table cabecera(
							nroresumen serial,
							nombre text,
							apellido text,
							domicilio text,
							nrotarjeta char(16),
							desde date,
							hasta date,
							vence date,
							total decimal(8,2)
						);`

var tablaDetalle string = `create table detalle(
							nroresumen serial,
							nrolinea int,
							fecha date,
							nombrecomercio text,
							monto decimal(7,2)
						);`

var tablaAlerta string = `create table alerta(
							nroalerta serial,
							nrotarjeta char(16),
							fecha timestamp,
							nrorechazo int,
							codalerta int, --0 rechazo, 1 compra 1min, 5 compra 5min, 32 límite
							descripcion text
						);`

var tablaConsumo string = `create table consumo(
							nrotarjeta char(16),
							codseguridad char(4),
							nrocomercio int,
							monto decimal(7,2)
						);`

						var pkCliente string =`alter table cliente add constraint cliente_pk   primary key (nrocliente);`	
						var pkTarjeta string =`alter table tarjeta add constraint tarjeta_pk   primary key (nrotarjeta);`						
						var pkComercio string =`alter table comercio add constraint comercio_pk   primary key (nrocomercio);`						
						var pkCompra string =`alter table compra add constraint compra_pk   primary key (nrooperacion);`						
						var pkRechazo string =`alter table rechazo add constraint rechazo_pk   primary key (nrorechazo);`						
						var pkAlerta string =`alter table alerta add constraint alerta_pk   primary key (nroalerta);`						
						var pkCabecera string =`alter table cabecera add constraint cabecera_pk   primary key (nroresumen);`						
						
						var fkTarjetaCliente string =`alter table tarjeta add constraint tarjeta_cliente_fk foreign key (nrocliente) references cliente(nrocliente);`
						var fkCompraTarjeta string =`alter table compra add constraint compra_tarjeta_fk foreign key (nrotarjeta) references tarjeta(nrotarjeta);`
						var fkCompraComercio string =`alter table compra add constraint compra_comercio_fk foreign key (nrocomercio) references comercio(nrocomercio);`
						var fkRechazoComercio string =`alter table rechazo add constraint rechazo_comercio_fk foreign key (nrocomercio) references comercio(nrocomercio);`
						var fkCabeceraTarjeta string =`alter table cabecera add constraint cabecera_tarjeta_fk foreign key (nrotarjeta) references tarjeta(nrotarjeta);`	
						var fkDetalleNroResumen string =`alter table detalle add constraint detalle_nroresumen_fk foreign key (nroresumen) references cabecera(nroresumen);`
						var fkAlertaTarjeta string =`alter table alerta add constraint alerta_tarjeta_fk foreign key (nrotarjeta) references tarjeta(nrotarjeta);`
						var fkAlertaRechazo string =`alter table alerta add constraint alerta_rechazo_fk foreign key (nrorechazo) references rechazo(nrorechazo);`
						var fkConsumoTarjeta string =`alter table consumo add constraint consumo_tarjeta_fk foreign key (nrotarjeta) references tarjeta(nrotarjeta);`
						var fkConsumoComercio string =`alter table consumo add constraint consumo_comercio_fk foreign key (nrocomercio) references comercio(nrocomercio);`
//hay

var inserts []string

func createDatabase() error {
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=postgres sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	_, err = db.Exec(`create database ` + nombreBase)
	return err
	/*if err != nil {
			return err;
	        //log.Fatal(err)
	    }*/
}
func crearElemento(tabla string) {
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname="+nombreBase+" sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	_, err = db.Exec(tabla)

	if err != nil {
		log.Fatal(err)
	}
}

func crearTablas() {
	crearElemento(tablaCliente)
	crearElemento(tablaTarjeta)
	crearElemento(tablaComercio)
	crearElemento(tablaCompra)
	crearElemento(tablaRechazo)
	crearElemento(tablaCierre)
	crearElemento(tablaCabecera)
	crearElemento(tablaDetalle)
	crearElemento(tablaAlerta)
	crearElemento(tablaConsumo)
}
func crearPks(){
	crearElemento(pkCliente)
	crearElemento(pkTarjeta)
	crearElemento(pkComercio)
	crearElemento(pkCompra)
	crearElemento(pkRechazo)
	crearElemento(pkAlerta)
	crearElemento(pkCabecera)
}
func crearFks(){
	crearElemento(fkTarjetaCliente)
	crearElemento(fkCompraTarjeta)
	crearElemento(fkCompraComercio)
	crearElemento(fkRechazoComercio)
	crearElemento(fkCabeceraTarjeta)
	crearElemento(fkDetalleNroResumen)
	crearElemento(fkAlertaTarjeta)
	crearElemento(fkAlertaRechazo)
	crearElemento(fkConsumoTarjeta)
	crearElemento(fkConsumoComercio)
}

func cargarClientes() {
	insert := []string{`insert into cliente values ( default,'maria'    ,'pereyra'  ,'san cristobal 3432'    ,43217687),
	                                               ( default,'monica'   ,'martinez' ,'moreno 1298'           ,15687075),
												   ( default,'juan'     ,'perez'    ,'gutierrez 626'         ,23790983),
												   ( default,'ramon'    ,'sanchez'  ,'escalabrini ortiz 1090',87864343),
												   ( default,'norma'    ,'perez'    ,'buenos aires 238'      ,53235767),
												   ( default,'german'   ,'gutierrez','san martin 2542'       ,89953344),
												   ( default,'susana'   ,'moran'    ,'vicente lopez 603'     ,93871610),
												   ( default,'mauro'    ,'gonzalez' ,'roca 2021'             ,25082038),
												   ( default,'ester'    ,'moreno'   ,'belgrano 1237'         ,23489459),
												   ( default,'jose'     ,'marquez'  ,'irigoyen 3482'         ,89840932),
												   ( default,'agustina' ,'fernandez','lavalle 2365'          ,41290943),
												   ( default,'jorge'    ,'villalba' ,'balbin 3424'           ,48933879),
												   ( default,'mauro'    ,'gomez'    ,'corrientes 405'        ,82367756),
												   ( default,'mirta'    ,'aguero'   ,'valentin gomez 3453'   ,76475900),
												   ( default,'tatiana'  ,'ruiz'     ,'rivadavia 6743'        ,57878947),
												   ( default,'maria'    ,'benites'  ,'pedro goyena 4983'     ,25479876),
												   ( default,'martin'   ,'gimenez'  ,'mujica 3892'           ,48356322),
												   ( default,'sebastian','romero'   ,'jujuy 587'             ,86654098),
												   ( default,'rodrigo'  ,'suarez'   ,'manuela pedraza 1238'  ,21875670),
												   ( default, 'elvis'   ,'presley'  ,'estamos ebrios 7865'   ,11334455);`}



	for _, valor := range insert {
		crearElemento(valor)
	}
}
func cargarComercios() {
	insert := []string{`insert into comercio values (default,'infotech'           ,'azcuenaga 261'     ,'1613',76456997),
	                                                (default,'orange'             ,'mexico 764'        ,'1614',43597809),
	                                                (default,'fruitfresh'         ,'san juan 2345'     ,'1302',98667566),
	                                                (default,'mariokids'          ,'cabildo 1587'      ,'3456',78856543),
	                                                (default,'todomadera'         ,'san pedro 819'     ,'6754',90675656),
	                                                (default,'fallabela'          ,'laprida 3765'      ,'2114',79755433),
	                                                (default,'dumbo'              ,'paunero 1421'      ,'5231',89876646),
	                                                (default,'petra'              ,'cordoba 4823'      ,'6422',87888667),
	                                                (default,'misol'              ,'alem 4231'         ,'3456',61203389),
	                                                (default,'farmaciasun'        ,'madero 2456'       ,'1302',87885437),
	                                                (default,'milmaravilla'       ,'paunero 2243'      ,'1104',56473829),
	                                                (default,'drugstoreBenito'    ,'libertador 1732'   ,'1104',98564644),
	                                                (default,'todomoda'           ,'arenales 3214'     ,'5764',78432434),
	                                                (default,'vitamina'           ,'mexico 3423'       ,'9893',77874638),
	                                                (default,'ferreteriasabaton'  ,'cabildo 4212'      ,'2175',87838458),
	                                                (default,'pizzeria donnatello','malabia 4313'      ,'1827',31243173),
	                                                (default,'kioscolamoraleja'   ,'floria 2145'       ,'8907',56464565),  
	                                                (default,'headeriacielo'      ,'motesdeoca 4134'   ,'2532',75488947),
	                                                (default,'cafelamorenita'     ,'suarez 5421'       ,'6413',45672416),
	                                                (default,'havannacafe'        ,'rodriguez peña 100','9053',32084778);`}
                                                
	for _, valor := range insert {
		crearElemento(valor)
	}
}
func cargarTarjetas() {
	insert := []string{`insert into tarjeta values ('5890484738473979' , 001, '201806', '201912', '4255', 20000.0, 'vigente'),
												   ('2437867765768766' , 002, '201609', '201910', '3211', 43550.8, 'suspendida'),
												   ('8779726845878626' , 003, '201704', '201911', '2153', 12442.2, 'vigente'), 
												   ('7874897575472754' , 003, '201701', '201912', '5667', 44211.4, 'vigente'),
												   ('1387862312358909' , 004, '201808', '201911', '7545', 16573.4, 'vigente'),
												   ('7398478497547476' , 005, '201810', '201911', '3124', 24131.0, 'vigente'),
												   ('9801278678450938' , 006, '201811', '201912', '4325', 54321.2, 'vigente'),
												   ('8974625254369553' , 007, '201603', '201912', '6734', 33212.8, 'vigente'),
												   ('9828687632029739' , 008, '201601', '201901', '7653', 98765.9, 'anulada'), 
												   ('7868762839899023' , 009, '201701', '201912', '1321', 32213.3, 'vigente'), 
												   ('9989075989120890' , 010, '201612', '201912', '2133', 12322.3, 'vigente'),
												   ('1232413347639803' , 011, '201512', '201911', '4323', 87273.5, 'suspendida'),
												   ('2126321684990348' , 012, '201703', '201912', '7432', 46673.7, 'suspendida'),
												   ('7837781236786786' , 013, '201903', '201912', '1214', 20000.1, 'vigente'),
												   ('4363419138578368' , 014, '201905', '201912', '2543', 13143.3, 'vigente'),
												   ('3236462556648093' , 015, '201902', '201912', '6452', 43534.5, 'vigente'),
			   									   ('4637527999178323' , 016, '201801', '201912', '3332', 11322.6, 'vigente'),
												   ('7981274937899290' , 017, '201804', '201911', '5467', 14000.6, 'anulada'),
												   ('1890897235989624' , 018, '201711', '201912', '6433', 30489.8, 'vigente'), 
												   ('9028904707089333' , 018, '201710', '201912', '0895', 35234.9, 'vigente'),
												   ('2186274698887487' , 019, '201802', '201912', '7934', 19046.4, 'vigente'),
												   ('3764457836281268' , 020, '201704', '201912', '8332', 12344.4, 'vigente');`}

	for _, valor := range insert {
		crearElemento(valor)
	}
}
func cargarCierres() {
	insert := []string{`insert into cierre values (2019,1 , 0, '2019-01-01', '2019-02-01', '2019-02-05'),
												  (2019,1 , 1, '2019-01-03', '2019-02-03', '2019-02-06'),
												  (2019,1 , 2, '2019-01-06', '2019-02-06', '2019-02-09'),
												  (2019,1 , 3, '2019-01-09', '2019-02-09', '2019-02-12'),
												  (2019,1 , 4, '2019-01-12', '2019-02-12', '2019-02-15'),
												  (2019,1 , 5, '2019-01-15', '2019-02-15', '2019-02-18'),
												  (2019,1 , 6, '2019-01-18', '2019-02-18', '2019-02-21'),
												  (2019,1 , 7, '2019-01-21', '2019-02-21', '2019-02-24'),
												  (2019,1 , 8, '2019-01-24', '2019-02-24', '2019-02-27'),
												  (2019,1 , 9, '2019-01-27', '2019-02-27', '2019-02-28'),
												  (2019,2 , 0, '2019-02-01', '2019-03-01', '2019-03-05'),
												  (2019,2 , 1, '2019-02-04', '2019-03-03', '2019-03-06'),
												  (2019,2 , 2, '2019-02-07', '2019-03-06', '2019-03-09'),
												  (2019,2 , 3, '2019-02-10', '2019-03-09', '2019-03-12'),
												  (2019,2 , 4, '2019-02-13', '2019-03-12', '2019-03-15'),
												  (2019,2 , 5, '2019-02-16', '2019-03-15', '2019-03-18'),
												  (2019,2 , 6, '2019-02-19', '2019-03-18', '2019-03-21'),
												  (2019,2 , 7, '2019-02-22', '2019-03-21', '2019-03-24'),
												  (2019,2 , 8, '2019-02-25', '2019-03-24', '2019-03-27'),
												  (2019,2 , 9, '2019-02-28', '2019-03-27', '2019-03-30'),
												  (2019,3 , 0, '2019-03-02', '2019-04-01', '2019-04-05'),
												  (2019,3 , 1, '2019-03-04', '2019-04-03', '2019-04-06'),
												  (2019,3 , 2, '2019-03-07', '2019-04-06', '2019-04-09'),
												  (2019,3 , 3, '2019-03-10', '2019-04-09', '2019-04-12'),
												  (2019,3 , 4, '2019-03-13', '2019-04-12', '2019-04-15'),
												  (2019,3 , 5, '2019-03-16', '2019-04-15', '2019-04-18'),
												  (2019,3 , 6, '2019-03-19', '2019-04-18', '2019-04-21'),
												  (2019,3 , 7, '2019-03-22', '2019-04-21', '2019-04-24'),
												  (2019,3 , 8, '2019-03-25', '2019-04-24', '2019-04-27'),
												  (2019,3 , 9, '2019-03-28', '2019-04-27', '2019-04-30'),
												  (2019,4 , 0, '2019-04-02', '2019-05-01', '2019-05-05'),
												  (2019,4 , 1, '2019-04-04', '2019-05-03', '2019-05-06'),
												  (2019,4 , 2, '2019-04-07', '2019-05-06', '2019-05-09'),
												  (2019,4 , 3, '2019-04-10', '2019-05-09', '2019-05-12'),
												  (2019,4 , 4, '2019-04-13', '2019-05-12', '2019-05-15'),
												  (2019,4 , 5, '2019-04-16', '2019-05-15', '2019-05-18'),
												  (2019,4 , 6, '2019-04-19', '2019-05-18', '2019-05-21'),
												  (2019,4 , 7, '2019-04-22', '2019-05-21', '2019-05-24'),
												  (2019,4 , 8, '2019-04-25', '2019-05-24', '2019-05-27'),
												  (2019,4 , 9, '2019-04-28', '2019-05-27', '2019-05-30'),
												  (2019,5 , 0, '2019-05-02', '2019-06-01', '2019-06-05'),
												  (2019,5 , 1, '2019-05-04', '2019-06-03', '2019-06-06'),
												  (2019,5 , 2, '2019-05-07', '2019-06-06', '2019-06-09'),
												  (2019,5 , 3, '2019-05-10', '2019-06-09', '2019-06-12'),
												  (2019,5 , 4, '2019-05-13', '2019-06-12', '2019-06-15'),
												  (2019,5 , 5, '2019-05-16', '2019-06-15', '2019-06-18'),
												  (2019,5 , 6, '2019-05-19', '2019-06-18', '2019-06-21'),
												  (2019,5 , 7, '2019-05-22', '2019-06-21', '2019-06-24'),
												  (2019,5 , 8, '2019-05-25', '2019-06-24', '2019-06-27'),
												  (2019,5 , 9, '2019-05-29', '2019-06-27', '2019-06-30'),
												  (2019,6 , 0, '2019-06-02', '2019-07-01', '2019-07-05'),
												  (2019,6 , 1, '2019-06-04', '2019-07-03', '2019-07-06'),
												  (2019,6 , 2, '2019-06-07', '2019-07-06', '2019-07-09'),
												  (2019,6 , 3, '2019-06-10', '2019-07-09', '2019-07-12'),
												  (2019,6 , 4, '2019-06-13', '2019-07-12', '2019-07-15'),
												  (2019,6 , 5, '2019-06-16', '2019-07-15', '2019-07-18'),
												  (2019,6 , 6, '2019-06-19', '2019-07-18', '2019-07-21'),
												  (2019,6 , 7, '2019-06-22', '2019-07-21', '2019-07-24'),
												  (2019,6 , 8, '2019-06-25', '2019-07-24', '2019-07-27'),
												  (2019,6 , 9, '2019-06-28', '2019-07-27', '2019-07-30'),
												  (2019,7 , 0, '2019-07-02', '2019-08-01', '2019-08-05'),
												  (2019,7 , 1, '2019-07-04', '2019-08-03', '2019-08-06'),
												  (2019,7 , 2, '2019-07-07', '2019-08-06', '2019-08-09'),
												  (2019,7 , 3, '2019-07-10', '2019-08-09', '2019-08-12'),
												  (2019,7 , 4, '2019-07-13', '2019-08-12', '2019-08-15'),
												  (2019,7 , 5, '2019-07-16', '2019-08-15', '2019-08-18'),
												  (2019,7 , 6, '2019-07-19', '2019-08-18', '2019-08-21'),
												  (2019,7 , 7, '2019-07-22', '2019-08-21', '2019-08-24'),
												  (2019,7 , 8, '2019-07-25', '2019-08-24', '2019-08-27'),
												  (2019,7 , 9, '2019-07-29', '2019-08-27', '2019-08-30'),
												  (2019,8 , 0, '2019-08-02', '2019-09-01', '2019-09-05'),
												  (2019,8 , 1, '2019-08-04', '2019-09-03', '2019-09-06'),
												  (2019,8 , 2, '2019-08-07', '2019-09-06', '2019-09-09'),
												  (2019,8 , 3, '2019-08-10', '2019-09-09', '2019-09-12'),
												  (2019,8 , 4, '2019-08-13', '2019-09-12', '2019-09-15'),
												  (2019,8 , 5, '2019-08-16', '2019-09-15', '2019-09-18'),
												  (2019,8 , 6, '2019-08-19', '2019-09-18', '2019-09-21'),
												  (2019,8 , 7, '2019-08-22', '2019-09-21', '2019-09-24'),
												  (2019,8 , 8, '2019-08-25', '2019-09-24', '2019-09-27'),
												  (2019,8 , 9, '2019-08-28', '2019-09-27', '2019-09-30'),
												  (2019,9 , 0, '2019-09-02', '2019-10-01', '2019-10-05'),
												  (2019,9 , 1, '2019-09-04', '2019-10-03', '2019-10-06'),
												  (2019,9 , 2, '2019-09-07', '2019-10-06', '2019-10-09'),
												  (2019,9 , 3, '2019-09-10', '2019-10-09', '2019-10-12'),
												  (2019,9 , 4, '2019-09-13', '2019-10-12', '2019-10-15'),
												  (2019,9 , 5, '2019-09-16', '2019-10-15', '2019-10-18'),
												  (2019,9 , 6, '2019-09-19', '2019-10-18', '2019-10-21'),
												  (2019,9 , 7, '2019-09-22', '2019-10-21', '2019-10-24'),
												  (2019,9 , 8, '2019-09-25', '2019-10-24', '2019-10-27'),
												  (2019,9 , 9, '2019-09-28', '2019-10-27', '2019-10-30'),
												  (2019,10, 0, '2019-10-02', '2019-11-01', '2019-11-05'),
												  (2019,10, 1, '2019-10-04', '2019-11-03', '2019-11-06'),
												  (2019,10, 2, '2019-10-07', '2019-11-06', '2019-11-09'),
												  (2019,10, 3, '2019-10-10', '2019-11-09', '2019-11-12'),
												  (2019,10, 4, '2019-10-13', '2019-11-12', '2019-11-15'),
												  (2019,10, 5, '2019-10-16', '2019-11-15', '2019-11-18'),
												  (2019,10, 6, '2019-10-19', '2019-11-18', '2019-11-21'),
												  (2019,10, 7, '2019-10-23', '2019-11-21', '2019-11-24'),
												  (2019,10, 8, '2019-10-25', '2019-11-24', '2019-11-27'),
												  (2019,10, 9, '2019-10-28', '2019-11-27', '2019-11-30'),
												  (2019,11, 0, '2019-11-02', '2019-12-01', '2019-12-05'),
												  (2019,11, 1, '2019-11-04', '2019-12-03', '2019-12-06'),
												  (2019,11, 2, '2019-11-07', '2019-12-06', '2019-12-09'),
												  (2019,11, 3, '2019-11-10', '2019-12-09', '2019-12-12'),
												  (2019,11, 4, '2019-11-13', '2019-12-12', '2019-12-15'),
												  (2019,11, 5, '2019-11-16', '2019-12-15', '2019-12-18'),
												  (2019,11, 6, '2019-11-19', '2019-12-18', '2019-12-21'),
												  (2019,11, 7, '2019-11-22', '2019-12-21', '2019-12-24'),
												  (2019,11, 8, '2019-11-25', '2019-12-24', '2019-12-27'),
												  (2019,11, 9, '2019-11-28', '2019-12-27', '2019-12-30'),
												  (2019,12, 0, '2019-12-02', '2020-01-01', '2020-01-05'),
												  (2019,12, 1, '2019-12-04', '2020-01-03', '2020-01-06'),
												  (2019,12, 2, '2019-12-07', '2020-01-06', '2020-01-09'),
												  (2019,12, 3, '2019-12-10', '2020-01-09', '2020-01-12'),
												  (2019,12, 4, '2019-12-13', '2020-01-12', '2020-01-15'),
												  (2019,12, 5, '2019-12-16', '2020-01-15', '2020-01-18'),
												  (2019,12, 6, '2019-12-19', '2020-01-18', '2020-01-21'),
												  (2019,12, 7, '2019-12-22', '2020-01-21', '2020-01-24'),
												  (2019,12, 8, '2019-12-25', '2020-01-24', '2020-01-27'),
												  (2019,12, 9, '2019-12-28', '2020-01-27', '2020-01-30');`}
	for _, valor := range insert {
		crearElemento(valor)
	}

}
func CargarBaseCompleta() error {
	err := createDatabase()
	if err == nil {
		crearTablas()
		crearPks()
		crearFks()
		cargarClientes()
		cargarComercios()
		cargarTarjetas()
		cargarCierres()
		return nil
	} else {
		fmt.Printf("La base esta creada")
		return err
	}

}
