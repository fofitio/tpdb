package BoltDB

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"os/exec"
	"strconv"
	"time"

	bolt "github.com/coreos/bbolt"
)

type Tarjeta struct {
	idTarjeta    int
	NroTarjeta   string
	NroCliente   int
	ValidaDesde  string
	ValidaHasta  string
	CodSeguridad string
	LimiteCompra float64
	Estado       string
}
type Cliente struct {
	NroCliente int
	Nombre     string
	Apellido   string
	Domicilio  string
	Telefono   string
}
type Comercio struct {
	NroComercio  int
	Nombre       string
	Domicilio    string
	CodigoPostal string
	Telefono     string
}
type Compra struct {
	Nrooperacion int
	Nrotarjeta   string
	Nrocomercio  int
	Fecha        string
	Monto        float64
	Pagado       bool
}

func cargarJsonClientes(db *bolt.DB) bool {
	clientes := []Cliente{
		Cliente{1, "Nicolas", "Lencinas", "Oncativo 1648", "1156895625"},
		Cliente{2, "Evelin", "Aragon", "Suares 1287", "1159898625"},
		Cliente{3, "Matias", "Dabrowski", "Libertad 522", "1140895625"}}

	for indice, valor := range clientes {
		data, err := json.Marshal(valor)

		if err != nil {
			log.Fatal(err)
		}
		CreateUpdate(db, "clientes", []byte(strconv.Itoa(indice+1)), data)

	}
	return true

}
func cargarJsonTarjeta(db *bolt.DB) {
	tarjetas := []Tarjeta{
		Tarjeta{1, "4846297846249672", 1, "201802", "202003", "987", 30000, "VIGENTE"},
		Tarjeta{2, "4401622421417328", 2, "201902", "202201", "403", 55000, "VIGENTE"},
		Tarjeta{3, "346052004053140", 3, "201702", "201908", "647", 10000, "VIGENTE"}}

	for indice, valor := range tarjetas {
		data, err := json.Marshal(valor)
		if err != nil {
			log.Fatal(err)
		}
		CreateUpdate(db, "tarjetas", []byte(strconv.Itoa(indice+1)), data)
	}
}
func cargarJsonComercio(db *bolt.DB) {
	comercios := []Comercio{
		Comercio{1, "Estancias Chiripa", "Encalada 113", "1405", "1134896754"},
		Comercio{2, "Opera Prima", "Pte. Perón 1398", "1613", "1155896754"},
		Comercio{3, "Mc'Donalds", "Vedia 3626", "1613", "1134456754"}}

	for indice, valor := range comercios {

		data, err := json.Marshal(valor)

		if err != nil {
			log.Fatal(err)
		}
		CreateUpdate(db, "comercios", []byte(strconv.Itoa(indice+1)), data)

	}

}
func cargarJsonCompra(db *bolt.DB) {
	compras := []Compra{Compra{1, "4846297846249672", 1, "", 312, true},
		Compra{2, "4401622421417328", 1, "", 686, true},
		Compra{3, "346052004053140", 3, "", 5000, true}}

	for indice, valor := range compras {

		data, err := json.Marshal(valor)

		if err != nil {
			log.Fatal(err)
		}
		CreateUpdate(db, "compras", []byte(strconv.Itoa(indice+1)), data)

	}
}
func CreateUpdate(db *bolt.DB, bucketName string, key []byte, val []byte) error {
	// abre transacción de escritura
	tx, err := db.Begin(true)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	b, _ := tx.CreateBucketIfNotExists([]byte(bucketName))

	err = b.Put(key, val)
	if err != nil {
		return err
	}

	// cierra transacción
	if err := tx.Commit(); err != nil {
		return err
	}

	return nil
}
func ReadUnique(db *bolt.DB, bucketName string, key []byte) ([]byte, error) {
	var buf []byte

	// abre una transacción de lectura
	err := db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(bucketName))
		buf = b.Get(key)
		return nil
	})

	return buf, err
}
func verTarjetas(db *bolt.DB, secuencia int) {
	var i int = 1
	clear()
	fmt.Printf("\nListado de Tarjetas:\n\n")
	fmt.Printf("%v%v%v%v%v%v%v\n",
		format("N°Tarjeta"),
		format("N°Cliente"),
		format("Valida Desde"),
		format("Valida Hasta"),
		format("CodSeguridad"),
		format("LimiteCompra"),
		format("Estado"))
	for i <= secuencia {
		data, err := ReadUnique(db, "tarjetas", []byte(strconv.Itoa(i)))
		var tarjeta Tarjeta

		err = json.Unmarshal(data, &tarjeta)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Printf("%v%v%v%v%v%v%v\n",
			format(tarjeta.NroTarjeta),
			format(fmt.Sprintf("%b", tarjeta.NroCliente)),
			format(tarjeta.ValidaDesde),
			format(tarjeta.ValidaHasta),
			format(tarjeta.CodSeguridad),
			format(fmt.Sprintf("%f", tarjeta.LimiteCompra)),
			format(tarjeta.Estado))

		i++
	}
}

func format(palabra string) (salida string) {

	largo := 22
	largo = largo - len(palabra)
	//fmt.Printf("%dYo soy el largo de %v\n\n", largo, palabra)
	retorno := ""
	retorno = retorno + palabra
	//fmt.Printf("%v\n\n", retorno)
	for j := 0; j <= largo; j++ {
		retorno += " "
	}
	return retorno

}
func verClientes(db *bolt.DB, secuencia int) {
	var i int = 1
	clear()
	fmt.Printf("\nListado de Clientes:\n\n")
	fmt.Printf("%v%v%v%v%v\n",
		format("N°Cliente"),
		format("Nombre"),
		format("Apellido"),
		format("Domicilio"),
		format("Telefono"))
	for i <= secuencia {
		data, err := ReadUnique(db, "clientes", []byte(strconv.Itoa(i)))
		var cliente Cliente

		err = json.Unmarshal(data, &cliente)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Printf("%v%v%v%v%v\n", format(fmt.Sprintf("%b", cliente.NroCliente)), format(cliente.Nombre), format(cliente.Apellido), format(cliente.Domicilio), format(cliente.Telefono))

		i++
	}
}
func verCompras(db *bolt.DB, secuencia int) {
	var i int = 1
	clear()
	fmt.Printf("\nCompras realizadas:\n\n")
	fmt.Printf("%v%v%v%v%v%v\n",
		format("N°Operacion"),
		format("N°Tarjeta"),
		format("N°Comercio"),
		format("Monto"),
		format("Fecha"),
		format("Pagado"))
	for i <= secuencia {
		data, err := ReadUnique(db, "compras", []byte(strconv.Itoa(i)))
		var compra Compra

		err = json.Unmarshal(data, &compra)
		if err != nil {
			log.Fatal(err)
		}
		pago := "NO"
		if compra.Pagado {
			pago = "SI"
		}

		fmt.Printf("%v%v%v%v%v%v\n",
			 format(fmt.Sprintf("%b", compra.Nrooperacion)),
			format(compra.Nrotarjeta),
			format(fmt.Sprintf("%b", compra.Nrocomercio)),
			format(fmt.Sprintf("%f", compra.Monto)),
			format(compra.Fecha),
			format(pago))

		i++
	}
}
func verComercios(db *bolt.DB, secuencia int) {
	var i int = 1
	clear()
	fmt.Printf("\nListado de Comercios:\n\n")

	fmt.Printf("%v%v%v%v%v\n",
		format("N°Comercio"),
		format("Nombre"),
		format("Domicilio"),
		format("CodigoPostal"),
		format("Telefono"))
	for i <= secuencia {
		data, err := ReadUnique(db, "comercios", []byte(strconv.Itoa(i)))
		var comercio Comercio

		err = json.Unmarshal(data, &comercio)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Printf("%v%v%v%v%v\n",
			format(fmt.Sprintf("%b", comercio.NroComercio)),
			format(comercio.Nombre),
			format(comercio.Domicilio),
			format(comercio.CodigoPostal),
			format(comercio.Telefono))

		i++
	}
}
func cargarBase(db *bolt.DB) {
	cargarJsonClientes(db)
	cargarJsonTarjeta(db)
	cargarJsonComercio(db)
	cargarJsonCompra(db)
}
func clear() {
	cmd := exec.Command("clear")
	cmd.Stdout = os.Stdout
	cmd.Run()
}
func imprimirEncabezado() {
	fmt.Printf("***********************************************************")
	fmt.Printf("\n**Bienvenido al Sistema de control de tarjetas de credito**\n")
	fmt.Printf("***********************************************************\n")
	fmt.Println(time.Now().Format("2006-01-02 Hora: 15:04:05\n"))
}
func Menu() {

	fmt.Printf("%s\n", "por conectar")
	db, err := bolt.Open("adm_tarjetas22.db", 0600, nil)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	if err != nil {
		log.Fatal(err)
	}
	cargarBase(db)

	bool := true

	a := "a"
	b := "b"
	c := "c"
	d := "d"
	q := "q"

	clear()

	imprimirEncabezado()

	for bool {
		var ingreso string

		fmt.Printf("\nBases no relacionales:\n\n")
		fmt.Printf("\nIngrese una de la siguientes opciones:\n\n")
		fmt.Printf("		a: Ver clientes\n")
		fmt.Printf("		b: Ver comercios\n")
		fmt.Printf("		c: Ver compras\n")
		fmt.Printf("		d: Ver tarjetas\n\n")
		fmt.Printf("		q: Volver al menu principal \n")
		fmt.Printf("\nSu seleccion: ")

		fmt.Scanf("%s", &ingreso)

		switch ingreso {
		case a:
			verClientes(db, 3)
		case b:
			verComercios(db, 3)
		case c:
			verCompras(db, 3)
		case d:
			verTarjetas(db, 3)
		case q:
			bool = false
			fmt.Printf("Saliendo del sumenu\n\n")
			clear()
			imprimirEncabezado()
		default:
			clear()
			fmt.Printf("Opcion invalida")

		}

	}

}
